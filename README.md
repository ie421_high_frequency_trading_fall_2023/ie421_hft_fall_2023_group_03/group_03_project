# KrakenBook3D

KrakenBook3D aims to provide a 3D visualization of all financial instruments in the Kraken Crypto Exchange. Historically, L2 Orderbooks have been used to display the bid ask spread as a function of price and time. Here we have developed our own orderbook to hold Kraken Data, Feedhandlers to pull from the public API, and a React App using the three.js library to visualize an order book for any currency in the Kraken Exchange as a function of price, depth and time.

## Installation 
### Running your Webserver 
```sh
cd my-react-app
npm install
npm start
```
If you run into any issues with dependencies or plugins when running `npm install`, you can resolve this by running 
```sh 
npm install --force
npm audit fix --force
```
*Note*: Using the `--force` flag forces packages to be installed and upgraded to resolve dependecy issues, which can lead to unexpected results, use with caution. 

## What exactly is a 3D OrderBook? 
<!-- Our goal is to develop a 3D visualization of an order book to display financial instruments of an exchange with price, quantity, and depth. Using the Kraken Websocket API, Three js library, and our implementation of an Order Book, we were able to create a realtime 3D visualization.  -->
An orderbook is an electronic list that collects all the buy and sell orders of a specific security or financial instrument to gauge market interest. Historically these are held in a 2D form via lists or graphs to demonstrate price levels and total quantities. There are many ways to visualize this in 2D such as: ![Screenshot](images/orderbooklist.png)

There are other ways to visualize this, another popular option is to use a depth chart, where you can plot price vs quantity in a graph format to easily identify liquidity and interest for each instrument without having to look as carefully, which can be seen below. 

![](images/depthchart.gif)

However, with any visualization of the orderbook in 2 dimensions, it is hard to grasp how liquidity of an orderbook changes over **time**. The goal of this project was to add this crucial component in instrument visualization by creating and representing an orderbook into the 3D space. We can accomplish this by using the [KrakenAPI](https://www.kraken.com/features/api-trading) and its cryptocurrency exchange to retrieve market data and the [three.js](https://threejs.org/) library to create 3D visuals for our orderbook. 

## Built With:

## Technologies and Tools

This project leverages a range of technologies and tools for both backend and frontend development, providing a robust and interactive experience.

### Backend Development

The backend infrastructure of this project is built using the following key technologies:

- **[Typescript](https://www.typescriptlang.org/):** A powerful superset of JavaScript that adds static type definitions, enabling more robust coding and error handling.
- **[Kraken API](https://www.kraken.com/features/api-trading):** Utilized for secure and reliable access to cryptocurrency trading data and features from the Kraken platform.
- **Websocket:** Implemented for real-time, bidirectional communication between the server and clients, ensuring instantaneous data transfer and updates.

### Frontend Development

The frontend interface is designed to be user-friendly and visually appealing, utilizing the following technologies:

- **HTML/CSS:** The core building blocks for creating the structure and style of the web interface.
- **[React](https://react.dev/):** A modern JavaScript library for building fast and reactive user interfaces for web applications.
- **[Three.js](https://threejs.org/):** A powerful JavaScript library and API used to create and display animated 3D computer graphics in a web browser.

---

## Orderbook Design
The design for our Kraken OrderBook can be found in `my-react-app/src/OrderBook.ts`. For this project, we have decided to use sorted lists for the buy and sell orders, where the first item in each list is the "best price," either the highest buy or lowest sell. We have categorized an interface Order, that stores the price, quantity, and order type (BUY/SELL), which are used to create the book itself. Currently we have functionality to add,remove, and match orders in our book. Add, well it adds the current Order to the book, filtered based on the order type. Remove does the opposite, it removes from the buyOrders or sellOrders lists depending on the order type of the current Order. 
![Screenshot](/images/bidaskspread.png)
MatchOrder restructures the orderbook based on the highest buys and lowest sells, this is commonly known as the bid-ask spread. A trade occurs in an orderbook when you "cross the bid-ask spread," that is when a buyer and a seller agree upon a price and agree to exchange. Based on the highest buys and lowest sells, our MatchOrder function determines when trades occur and reorganizes our buy and sell orders based on the quantity. 

## KrakenFeedHandler: Real-Time Data Handling
Our feedhandler is found in `my-react-app/src/Feedhandler/FeedHandler.ts`. 

---

The `KrakenFeedHandler` class is a crucial component of our application, designed to handle real-time cryptocurrency data from the Kraken Exchange. It establishes a WebSocket connection to Kraken's public feed, enabling the live tracking of order book changes and trade executions.

### Key Features

- **WebSocket Integration:** Utilizes WebSocket for a persistent, low-latency connection to Kraken's live trading feed, ensuring real-time data updates.
- **Dynamic Subscription:** Capable of subscribing to different trading pairs, it allows users to monitor various cryptocurrency markets.
- **Order Book Management:** Efficiently processes and updates the order book data. It adds new orders, updates existing ones, and removes filled or canceled orders.
- **Real-Time Visualization:** Seamlessly integrates with the `BookAnimation` class to reflect live changes in the market, providing an interactive 3D visualization of the order book.
- **Trade Event Handling:** Captures trade events, providing insights into market transactions as they happen.

### Technical Workflow

1. **Connection Management:** Establishes and manages the WebSocket connection to `wss://ws.kraken.com`. It includes methods to connect and disconnect, ensuring proper resource management.
2. **Data Processing:** Upon receiving messages, it decodes and processes the data, distinguishing between order book updates and trade events.
3. **State Update:** Updates the state of the `OrderBook` instance and triggers a refresh in the 3D visualization whenever there's a significant change in the market data.
4. **Error Handling:** Implements robust error handling and logging mechanisms, enhancing the reliability and maintainability of the application.

### Utilization

This class plays a pivotal role in keeping the application synced with live market data. It's designed for scalability and can be adapted to different market data sources or trading platforms with similar WebSocket APIs.

---

## Animations
### BookAnimation Class Overview
Our BookAnimation is found in `my-react-app/src/3DBookAnimation.ts`. 
Our ThreeScene is found in `my-react-app/src/ThreeScene.tsx`. 

The `BookAnimation` class is a core part of our visualization tool, bringing real-time cryptocurrency market data to life using 3D graphics rendered via [three.js](https://threejs.org/). This class is responsible for creating, updating, and rendering the visual representation of an order book obtained from cryptocurrency exchanges like Kraken.

#### Core Functionalities

- **3D Scene and Camera Setup:** Leverages `three.js` for creating a dynamic 3D scene. Utilizes `SceneManager` and `CameraManager` for managing the scene and camera perspectives.
- **Order Book Integration:** Tightly integrates with the `OrderBook` class, displaying live buy and sell orders in a visually intuitive format.
- **Dynamic Visualization:** Capable of recalculating and redrawing the 3D representation of the order book as new data arrives. 
- **Precision Handling:** Implements methods for precision rounding and tick size adjustments to ensure accurate representation of market data.
- **User Interaction:** Facilitates interaction with the visualization, like zooming and rotating the view, to provide an immersive experience.

#### Technical Details

1. **Initialization:** Sets up the 3D scene, camera, lighting, and other essential elements needed for the visualization.
2. **Order Book Data Processing:** Transforms raw order book data into a format suitable for 3D visualization, considering aspects like depth, tick size, and order types.
3. **Rendering Logic:** Draws and updates the 3D objects representing buy and sell orders, adjusting their size and color based on real-time market changes.
4. **UI Components:** Incorporates interactive elements like labels and animations to enhance the user's understanding of the data.

### ThreeScene Component Overview

The `ThreeScene` component is a React-based interface that provides an interactive environment to visualize real-time cryptocurrency market data using 3D graphics.

#### Key Features

- **Integration with Web Technologies:** Seamlessly combines HTML/CSS for layout, React for state management, and three.js for 3D rendering.
- **Dynamic Market Data Fetching:** Utilizes custom hooks and asynchronous calls to fetch live data from BitMEX and Kraken exchanges.
- **Real-Time Data Visualization:** Connects to `BookAnimation` for rendering the live cryptocurrency market data in a 3D scene.
- **Responsive Design:** Ensures a fluid user experience with dynamic window resizing and event handling.
- **Interactive UI:** Offers a dropdown for selecting different cryptocurrency pairs and displays the 3D market visualization accordingly.

#### Usage and Functionality

1. **Initialization:** On component mount, fetches available instruments and sets up the 3D scene and renderer.
2. **Data Handling:** Manages state for selected instruments and order book, updating the visualization as new data is received.
3. **User Interaction:** Provides a dropdown menu for users to select different cryptocurrency pairs and dynamically updates the 3D visualization based on the selection.
4. **Resource Management:** Cleans up and disconnects from data feeds and event listeners on component unmount.

---

## Visuals

![Screenshot](images/visual2.jpg)
![Screenshot](images/visual.png)

## Features
- Shows Kraken Crypto Exchange bid ask spread in real time. 
- Orderbook animations to show the bid ask spread as a function of price, quantity, and depth. 
- Dropdown menu to filter data based on currency symbols, such as 'ETH/USDT' 

## Potential future ideas

- We would like to add more exchanges done via adding new feedhandlers and creating a dropdown option for exchanges. 
- Add frontend functionality to visualize exchanges around the world, showing trades executing from different locations.
- Add historical menu to see visualizations for past market data, such as the flash crash. 
- Edit animation to center the market in our camera. 
- Add trade indicators or have a status bar for trades occuring
- Optimize orderbook and animations to run smoother for the client. 

## Teammates

**Kenny Kim (kk67@illinois.edu)**
- Hello my name is Kenny Kim a Junior in Computer Engineering and Math at the University of Illinois at Urbana-Champaign interested in Computer Vision and Quantitative Finance. I have worked in the industry as a Software Development Engineering Intern at Amazon with AWS Simspace Weaver where I did full-stack development and have taken Computational Photography, Machine Learning, Computer Systems, etc here at UIUC. Outside of work, I like to play music and rock climb. 

- My LinkedIn is located here: https://www.linkedin.com/in/kennydkim/

**Ritul K. Soni (ritulsonicareer@gmail.com)**
- Hello my name is Ritul K. Soni and I am a Chicago Based Software Developer currently a Junior studying Computer Science & Economics at The University of Illinois Urbana Champaign. Driven by a profound passion for software development in the realm of quantitative finance, I am eager to leverage my educational background in Economics and Computer Science to excel in this field. Additionally, my interests extend to the domains of AI, machine learning, and data visualization, fueling my enthusiasm to explore and contribute to these cutting-edge areas.


- My Website is located here: https://ritulsoni.com/
- My LinkedIn is located here: https://www.linkedin.com/in/RitulSoni/


**Joe Morrissey (jrm15@illinois.edu)**
- Hi, I am Joe Morrissey, a Junior Computer Science at the University of Illinois Urbana-Champaign. I have taken classes related to Computational Photography, Deep Learning, Algorithms, and Computer Architecture. I have industry experience as a Software Engineer Intern at Novaspect and currently work as a course assistant for the Probability and Statistics class here at UIUC. In my free time, I like to watch tv/movies and play video games with friends. My linkedin is: https://www.linkedin.com/in/joseph-morrissey-26b8a7228/

**Justin Kim (jwk8@illinois.edu)** 
- Hello my name is Justin Kim, a Junior Computer Science at the University of Illinois at Urbana-Champaign. I am interested in AI/ML and Quantitative Finance and I have taken courses in Machine Learning, Algorithms, Computer Systems, etc. In my free time, I like to play video games with friends.

- My LinkedIn is located here: https://www.linkedin.com/in/justin-kim-23355a223/
